<?php
namespace Config\Config;

class BalanceSheetDefaultConfig
{
    const CATEGORY = [
        'NULL' => 0,
        'CURRENT_ASSETS' => 1, //流动资产
        'NOT_CURRENT_ASSETS' => 2, //非流动资产
        'CURRENT_DEBTS' => 3, //流动负债
        'NOT_CURRENT_DEBTS' => 4, //非流动负债
        'OWNERSHIP_INTEREST' => 5 //所有者权益
    ];

    const TOTAL_TYPE = [
        'NULL' => 0,
        'CURRENT_ASSETS' => 1, //流动资产合计
        'NOT_CURRENT_ASSETS' => 2, //非流动资产合计
        'ASSETS' => 3, //资产合计
        'CURRENT_DEBTS' => 4, //流动负债合计
        'NOT_CURRENT_DEBTS' => 5, //非流动负债合计
        'DEBTS' => 6, //负债合计
        'OWNERSHIP_INTEREST' => 7, //所有者权益合计
        'OWNERSHIP_INTEREST_AND_DEBTS' => 8 //负债和所有者权益合计
    ];

    const  METHOD = [
        'NULL' => 0,
        'SUM' => 1,
        'DIFF' => 2
    ];

    const DEFAULT_BALANCE_SHEET = [
        [
            'assetName' => '流动资产',
            'assetLineNumber' => 0,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '流动负债',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  货币资金',
            'assetLineNumber' => 1,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1001 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1002 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1012 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  短期借款',
            'debtLineNumber' => 31,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2001 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  短期投资',
            'assetLineNumber' => 2,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1101 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应付票据',
            'debtLineNumber' => 32,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2201 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  应收票据',
            'assetLineNumber' => 3,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1121 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应付账款',
            'debtLineNumber' => 33,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2202 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  应收账款',
            'assetLineNumber' => 4,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1122 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  预收账款',
            'debtLineNumber' => 34,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2203 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  预付账款',
            'assetLineNumber' => 5,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1123 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应付职工薪酬',
            'debtLineNumber' => 35,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2211 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  应收股利',
            'assetLineNumber' => 6,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1131 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应交税费',
            'debtLineNumber' => 36,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2221 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  应收利息',
            'assetLineNumber' => 7,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1132 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应付利息',
            'debtLineNumber' => 37,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2231 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  其他应收款',
            'assetLineNumber' => 8,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1221 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  应付利润',
            'debtLineNumber' => 38,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2232 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  存货',
            'assetLineNumber' => 9,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1401 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1402 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1403 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1404 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1405 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1407 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1408 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1411 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1421 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4001 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4101 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4401 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4403 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  其他应付款',
            'debtLineNumber' => 39,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2241 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '    其中：原材料',
            'assetLineNumber' => 10,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1403 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  其他流动负债',
            'debtLineNumber' => 40,
            'debtCategory' => self::CATEGORY['CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '    在产品',
            'assetLineNumber' => 11,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                4001 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4101 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4401 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                4403 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  流动负债合计',
            'debtLineNumber' => 41,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['CURRENT_DEBTS'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '    库存商品',
            'assetLineNumber' => 12,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1405 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '非流动负债',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '    周转材料',
            'assetLineNumber' => 13,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1411 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  长期借款',
            'debtLineNumber' => 42,
            'debtCategory' => self::CATEGORY['NOT_CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2501 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  其他流动资产',
            'assetLineNumber' => 14,
            'assetCategory' => self::CATEGORY['CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '  长期应付款',
            'debtLineNumber' => 43,
            'debtCategory' => self::CATEGORY['NOT_CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2701 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  流动资产合计',
            'assetLineNumber' => 15,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['CURRENT_ASSETS'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '  递延收益',
            'debtLineNumber' => 44,
            'debtCategory' => self::CATEGORY['NOT_CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                2401 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '非流动资产',
            'assetLineNumber' => 0,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '  其他非流动负债',
            'debtLineNumber' => 45,
            'debtCategory' => self::CATEGORY['NOT_CURRENT_DEBTS'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  长期债券投资',
            'assetLineNumber' => 16,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1501 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  非流动负债合计',
            'debtLineNumber' => 46,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NOT_CURRENT_DEBTS'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  长期股权投资',
            'assetLineNumber' => 17,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1511 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '负债合计',
            'debtLineNumber' => 47,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['DEBTS'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  固定资产原价',
            'assetLineNumber' => 18,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1511 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  减：累计折旧',
            'assetLineNumber' => 19,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1602 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  固定资产账面价值',
            'assetLineNumber' => 20,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['DIFF'],
            'assetSubject' => [
                1601 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1602 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  在建工程',
            'assetLineNumber' => 21,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1604 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  工程物资',
            'assetLineNumber' => 22,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1605 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  固定资产清理',
            'assetLineNumber' => 23,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1606 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  生产性生物资产',
            'assetLineNumber' => 24,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['DIFF'],
            'assetSubject' => [
                1621 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1622 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '所有者权益（或股东权益）',
            'debtLineNumber' => 0,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '  无形资产',
            'assetLineNumber' => 25,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['DIFF'],
            'assetSubject' => [
                1701 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                1702 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  实收资本（或股本）',
            'debtLineNumber' => 48,
            'debtCategory' => self::CATEGORY['OWNERSHIP_INTEREST'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                3001 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  开发支出',
            'assetLineNumber' => 26,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                4301 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  资本公积',
            'debtLineNumber' => 49,
            'debtCategory' => self::CATEGORY['OWNERSHIP_INTEREST'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                3002 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  长期待摊费用',
            'assetLineNumber' => 27,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1801 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  盈余公积',
            'debtLineNumber' => 50,
            'debtCategory' => self::CATEGORY['OWNERSHIP_INTEREST'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                3101 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  其他非流动资产',
            'assetLineNumber' => 28,
            'assetCategory' => self::CATEGORY['NOT_CURRENT_ASSETS'],
            'assetTotalType' => self::TOTAL_TYPE['NULL'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [
                1901 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ],
            'debtName' => '  未分配利润',
            'debtLineNumber' => 51,
            'debtCategory' => self::CATEGORY['OWNERSHIP_INTEREST'],
            'debtTotalType' => self::TOTAL_TYPE['NULL'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => [
                3103 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ],
                3104 => [
                    'endBalance' => 0,
                    'yearBeginBalance' =>0
                ]
            ]
        ],
        [
            'assetName' => '  非流动资产合计',
            'assetLineNumber' => 29,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['NOT_CURRENT_ASSETS'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '  所有者权益合计',
            'debtLineNumber' => 52,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['OWNERSHIP_INTEREST'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
        [
            'assetName' => '资产总计',
            'assetLineNumber' => 30,
            'assetCategory' => self::CATEGORY['NULL'],
            'assetTotalType' => self::TOTAL_TYPE['ASSETS'],
            'assetEndBalance' => 0,
            'assetYearBeginBalance' => 0,
            'assetCalculationMethod' => self::METHOD['SUM'],
            'assetSubject' => [],
            'debtName' => '负债和所有者权益总计',
            'debtLineNumber' => 53,
            'debtCategory' => self::CATEGORY['NULL'],
            'debtTotalType' => self::TOTAL_TYPE['OWNERSHIP_INTEREST_AND_DEBTS'],
            'debtEndBalance' => 0,
            'debtYearBeginBalance' => 0,
            'debtCalculationMethod' => self::METHOD['SUM'],
            'debtSubject' => []
        ],
    ];
}
