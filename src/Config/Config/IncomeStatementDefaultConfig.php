<?php
namespace Config\Config;

class IncomeStatementDefaultConfig
{
    const CATEGORY = [
        'NULL' => 0,
        'OPERATING_INCOME' => 1, //营业收入
        'OPERATING_PROFIT' => 2, //营业利润
        'TOTAL_PROFIT' => 3, //利润总额
        'NET_PROFIT' => 4, //净利润
    ];

    const DEFAULT_INCOME_STATEMENT = [
        1 => [
            'projectName' => '一、营业收入',
            'category' => self::CATEGORY['OPERATING_INCOME'],
            'lineNumber' => 1,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5001 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                5051 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        2 => [
            'projectName' => '  减：营业成本',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 2,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5001 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                5051 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        3 => [
            'projectName' => '  税金及附加',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 3,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5403 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        4 => [
            'projectName' => '    其中：消费税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 4,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222113 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        5 => [
            'projectName' => '    营业税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 5,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222112 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        6 => [
            'projectName' => '    城市维护建设税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 6,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222117 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        7 => [
            'projectName' => '    资源税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 7,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222114 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        8 => [
            'projectName' => '    应交土地增值税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 8,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222116 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        9 => [
            'projectName' => '    城镇土地使用税、房产税、车船税、印花税',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 9,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222118 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222119 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222120 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222125 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        10 => [
            'projectName' => '    教育费附加、矿产资源补偿费、排污费',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 10,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                222122 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222123 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222124 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                222126 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        11 => [
            'projectName' => '  销售费用',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 11,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5601 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        12 => [
            'projectName' => '  其中：商品维修费',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 12,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560110 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        13 => [
            'projectName' => '  广告费和业务宣传费',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 13,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560115 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ],
                560116 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        14 => [
            'projectName' => '  管理费用',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 14,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5602 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        15 => [
            'projectName' => '    其中：开办费',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 15,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560209 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        16 => [
            'projectName' => '    业务招待费',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 16,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560102 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        17 => [
            'projectName' => '    研究费用',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 17,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560210 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        18 => [
            'projectName' => '  财务费用',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 18,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5603 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        19 => [
            'projectName' => '    其中：利息费用（收入以“-”号填列）',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 19,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                560301 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        20 => [
            'projectName' => '  加：投资收益（亏损以“-”号填列）',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 20,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5111 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        21 => [
            'projectName' => '二、营业利润（亏损以“-”号填列）',
            'category' => self::CATEGORY['OPERATING_PROFIT'],
            'lineNumber' => 21,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => []
        ],
        22 => [
            'projectName' => '  加：营业外收入',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 22,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5301 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        23 => [
            'projectName' => '    其中：政府补助',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 23,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                530102 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        24 => [
            'projectName' => '  减：营业外支出',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 24,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5711 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        25 => [
            'projectName' => '    其中：坏账损失',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 25,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                571105 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        26 => [
            'projectName' => '    无法收回的长期债券投资损失',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 26,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                571107 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        27 => [
            'projectName' => '    无法收回的长期股权投资损失',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 27,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                571108 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        28 => [
            'projectName' => '    自然灾害等不可抗力因素造成的损失',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 28,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                571109 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        29 => [
            'projectName' => '    税收滞纳金',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 29,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                571110 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        30 => [
            'projectName' => '三、利润总额（亏损总额以“-”号填列）',
            'category' => self::CATEGORY['TOTAL_PROFIT'],
            'lineNumber' => 30,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => []
        ],
        31 => [
            'projectName' => '  减：所得税费用',
            'category' => self::CATEGORY['NULL'],
            'lineNumber' => 31,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => [
                5801 => [
                    'yearTotal' => 0,
                    'monthTotal' => 0
                ]
            ]
        ],
        32 => [
            'projectName' => '  四、净利润（净亏损以“-”号填列）',
            'category' => self::CATEGORY['NET_PROFIT'],
            'lineNumber' => 32,
            'yearTotal' => 0,
            'monthTotal' => 0,
            'subject' => []
        ]
    ];
}
